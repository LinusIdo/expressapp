var express = require('express');
var router = express.Router({
    caseSensitive: true,
    strict: true
});
router.get('/', function (req, res, next) {
    res.render('contactus', { title: 'Contactus', error: '' });
});
router.get('/test', function (req, res, next) {
    res.render('contactus', { title: 'Contactus', error: '' });
});
module.exports = router;
