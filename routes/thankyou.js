var express = require('express');
var router = express.Router({ strict: true, caseSensitive: true });

var fs = require('fs');
router.post('/', function (req, res) {
    req.assert('fullname', 'Full Name is required.').notEmpty();
    req.assert('message', 'Message is required.').notEmpty();
    var errors = req.validationErrors();
    if (errors) res.render('contactus', { title: 'Contactus', error: errors });
    else {
        fs.writeFile(__dirname + '/mydata.txt', 'Full Name:' + req.body.fullname + ', Type:' + req.body.type + ', Message:' + req.body.message, 'utf8');
        res.render('thankyou', { title: 'Thank You!', name: req.body.fullname });
    }
});
module.exports = router;
